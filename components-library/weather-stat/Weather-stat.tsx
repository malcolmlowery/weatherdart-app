import * as React from 'react';
import './weather-stat.scss';

interface WeatherStatI {
  className?: string;
  constant: number;
  icon?: string;
  label: string;
  weatherSymbol?: string;
}

const WeatherStat: React.SFC<WeatherStatI> = ({ 
  className, 
  constant, 
  icon, 
  label, 
  weatherSymbol 
  }) => (

  <div className='stat-container'>
    <div className={`icon-container ${className}`}>
      <span className={icon}></span>
    </div>
    <label className='label'>{label}</label>
    <div className={`constant-container ${className}`}>
      <label className='constant'>
        {constant}
        <label>{weatherSymbol}</label>
      </label>
    </div>
  </div>

)

export default WeatherStat;