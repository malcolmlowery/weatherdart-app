import * as React from 'react';
import './card.scss';

interface CardI {
  bodyContent: React.ReactNode
  className?: string;
  headerContent?: React.ReactNode;
  headerVisible?: boolean;
}

const Card: React.SFC<CardI>  = ({ 
  bodyContent,
  className,
  headerContent,
  headerVisible 
  }) => (

  <div className={`card ${className}`}>
    { 
      headerVisible 
      && 
      <div className='header'>
        <label>{headerContent}</label>
      </div>
    }
    <div className='body'>
      {bodyContent}
    </div>
  </div>
  
);

export default Card;