import * as React from 'react';
import * as ReactDOM from 'react-dom';
import '../styles/Global-Styles.scss';

import App from '../src/App';

ReactDOM.render(<App />, document.getElementById('root'));
