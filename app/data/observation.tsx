export const data = {
  "success": true,
  "error": false,
  "response": {
    "ob": {
      "feelslikeF": 75,
      "dewpointF": 69,
      "humidity": 85,
      "pressureIN": 29.8,
      "windMPH": 7,
      "windDir": "E",
      "windGustKPH": 19,
      "visibilityMI": 11,
      "sunrise": 1577275516,
      "sunset": 1577313342,
      "weatherPrimary": "Partly Cloudy"
    }
  }
}