import * as React from 'react';
import Conditons from './components/conditions-view/Conditions';
import './App.scss'
import Map from './components/map-view/Map';

const App = () => (
  <div style={{ display: 'flex', fontSize: 24, justifyContent: 'space-evenly', width: '100%', flexWrap: 'wrap'}}>
    <Conditons />
    <Map />
  </div>
);

export default App;