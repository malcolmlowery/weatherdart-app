import * as React from 'react';
import { Tab, Tabs, Typography } from '@material-ui/core';
import './conditions.scss';

// Imported components
import Card from '../../../../components-library/card/Card';
import Observation from './observation/Observation';

interface TabPanelProps {
  children?: React.ReactNode;
  dir?: string;
  index: number;
  value: number;
}

const TabPanel = (props: TabPanelProps) => {
  const { children, index, value } = props;

  return (
    <Typography 
      component='div'
      role='tabpanel'
      hidden={value !== index}
      id={`full-width-tab-${index}`}
    >
      { value === index && <>{children}</> }
    </Typography>
  )
}

const TabView = () => {
  const [value, setValue] = React.useState(0);

  const handleChange = (event: React.ChangeEvent<{}>, newValue: number) => {
    setValue(newValue)
  }

  return (
    <div className='tabs-container'>
        <Tabs
          className='tabs'
          value={value}
          variant='fullWidth'
          onChange={handleChange}
        >
          <Tab className='tab' label='Observation' />
          <Tab label='Weather Alerts' />
        </Tabs>
        <TabPanel value={value} index={0} children={<Observation />} />
        <TabPanel value={value} index={1} children={'Tab 2'} />
    </div>
  )
}

const Conditons = () => {
  return (
    <div className='conditions'>
      <Card 
        bodyContent={<TabView />} 
        className={'card-container'}
        headerContent={'header'} 
        headerVisible={false} 
      />
    </div>
  )
}

export default Conditons;
