import * as React from 'react';
import './observation.scss';

// Imported Components
import WeatherIcon from '../../weather-icon/Weather-icon';
import WeatherStat from '../../../../../components-library/weather-stat/Weather-stat';

// Imported Test Data
import { data } from '../../../../data/observation';

const Observation = () => {
  const ob = data.response.ob;
  
  const weatherStats = () => {
    return (
      <div className='datail'>
        <WeatherStat 
          className='dewpoint'
          constant={ob.dewpointF}
          icon='wi wi-thermometer'  
          label='Dewpoint' 
          weatherSymbol='°'
        />
        <WeatherStat 
          className='humidity'
          constant={ob.humidity}
          icon='wi wi-humidity'  
          label='Humidity' 
          weatherSymbol='%'
        />
        <WeatherStat 
          className='wind'
          constant={ob.windMPH}
          icon='wi wi-strong-wind'  
          label='Wind' 
          weatherSymbol={` ${ob.windDir}`}
        />
        <WeatherStat 
          className='visibility'
          constant={ob.visibilityMI}
          icon='wi wi-cloudy-windy'  
          label='Visibility' 
          weatherSymbol=' miles'
        />
        <WeatherStat 
          className='pressure'
          constant={ob.pressureIN}
          icon='wi wi-barometer'  
          label='Pressure' 
          weatherSymbol='in'
        />
      </div>
    )
  }

  return (
    <div className='observation'>
      <div className='content'>

        <div className='temps-container'>
          <label className='label'>Currently Feels Like</label>
          <div className='temps'>
            <label className='low'>Low: 62°</label>
            <label className='current'>75<span>°</span></label>
            <label className='high'>High: 87°</label>
          </div>
          <WeatherIcon className='icon' icon={'wi wi-day-sprinkle'} />
          <label className='summary'>Sunny with a chance of thunderstorms</label>
        </div>
        <hr className='line-break'/>
        <div className='details-container'>
          {weatherStats()}
        </div>

      </div>
    </div>
  )

}

export default Observation;