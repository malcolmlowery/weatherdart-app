import * as React from 'react';
import './weather-icon.scss';

interface IconI {
  icon: string;
  className: string;
}

const WeatherIcon: React.SFC<IconI> = ({ className, icon }) => <i className={`${className} ${icon}`} />

export default WeatherIcon;