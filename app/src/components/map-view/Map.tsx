import * as React from 'react';
import AerisWeather from '@aerisweather/javascript-sdk';
import '@aerisweather/javascript-sdk/dist/styles/sass/styles.scss';
import './map.scss';
import Card from '../../../../components-library/card/Card';

const aeris = new AerisWeather('fq8uI1VHSI7Loh2bkL5ya', 'tuSGssbfEK8wCLzRf3U7TZiViN8flm8HbnqSb2rm')
aeris.views().then((views) => {
  return new views.InteractiveMap(document.getElementById('map'), { 
    strategy: 'google',
    accessToken: 'AIzaSyC41uLbVxlGXVylO7uF65p7OJ0UIX4SduE',
    zoom: 10,
    center: {
      lat: 26.1224,
      lon: -80.1373
    },
    layers: [{
      layer: 'radar',
      options: {
        style: {
          opacity: 0.75
        }
      }
    }],
  })
});

const Map = () => {
  
  return (
    <Card
      bodyContent={<MapView />} 
      className='map-container'
      headerVisible={true}
      headerContent='Fort Lauderdale, FL'
    />
  )
}

const MapView = () => {
  return (
    <div className='map-view-container'>
      <div id='map'></div>
    </div>
  )
}

export default Map;